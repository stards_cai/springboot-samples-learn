package com.hyh.manager;

import cn.hutool.json.JSONUtil;
import com.hyh.dto.WebLog;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimerTask;

/**
 * 异步工厂
 * @author Summerday
 */
public class AsyncFactory {

    private static final Logger log = LoggerFactory.getLogger(AsyncFactory.class);


    public static TimerTask saveLog(WebLog webLog){
        return new TimerTask() {
            @SneakyThrows
            @Override
            public void run() {
                // 模拟插入数据库操作
                Thread.sleep(10 * 1000);
                log.info("{}",JSONUtil.parse(webLog));
            }
        };
    }

}
