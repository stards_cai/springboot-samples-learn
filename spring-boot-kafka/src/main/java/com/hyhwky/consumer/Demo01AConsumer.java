package com.hyhwky.consumer;

import com.hyhwky.message.Demo01Msg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author Summerday
 */

@Component
@Slf4j
public class Demo01AConsumer {

    @KafkaListener(topics = Demo01Msg.TOPIC, groupId = "demo01-consumer-group-" + Demo01Msg.TOPIC)
    public void onMessage(Demo01Msg msg) {
        log.info("线程编号 : {}, 消息内容 : {}", Thread.currentThread().getId(), msg);
    }

}
