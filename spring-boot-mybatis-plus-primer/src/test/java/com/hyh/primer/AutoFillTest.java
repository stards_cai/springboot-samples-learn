package com.hyh.primer;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hyh.primer.entity.User;
import com.hyh.primer.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class AutoFillTest {

    @Resource
    UserService userService;

    @Test
    void insert() {

        User user = new User();
        user.setName("hyh2");
        boolean save = userService.save(user);
        System.out.println(save);

    }

    @Test
    void update() {
        int version = 2;
        User user = new User();
        user.setId(1320037517763842049L);
        user.setName("sm2");
        user.setVersion(version);
        boolean b = userService.updateById(user);
        System.out.println(b);

    }

}
