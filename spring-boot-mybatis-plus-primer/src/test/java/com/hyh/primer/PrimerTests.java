package com.hyh.primer;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hyh.primer.entity.User;
import com.hyh.primer.mapper.UserMapper;
import com.hyh.primer.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class PrimerTests {

    @Resource
    UserService userService;

    @Test
    void contextLoads() {

        List<Object> objects = userService.listObjs();
        for (Object object : objects) {
            System.out.println(object);
        }
    }

    @Test
    void logicDelete(){
        //UPDATE user SET deleted=1 WHERE deleted=0 AND (name = ?)
        boolean remove = userService.remove(new QueryWrapper<User>().eq("name", "summer"));
        System.out.println(remove);

    }
    @Test
    void select(){
        //SELECT id,name,deleted FROM user WHERE deleted=0
        List<User> list = userService.list(new QueryWrapper<User>());
        System.out.println(list);
    }

    @Test
    public void updateById(){
        //UPDATE user SET age=? WHERE id=? AND deleted=0
        User user = new User();
        user.setId(1320008348799713282L);
        user.setAge(20);
        boolean b = userService.updateById(user);
        System.out.println(b);
    }

    @Test
    void mySelect(){
        List<User> list = userService.mySelectList();
        System.out.println(list);
    }



}
