package com.hyh.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hyh.mybatisplus.entity.User;
import com.hyh.mybatisplus.mapper.UserMapper;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class InsertTest {

    @Resource
    UserMapper mapper;

    @Test
    void insert() {
        User user = new User();
        user.setName("auto");
        user.setEmail("12222@qq.com");
        user.setRemark("备注信息");
        user.setAge(20);
        user.setCreateTime(new Date());
        int rows = mapper.insert(user);
        System.out.println("影响记录数: " + rows);
        System.out.println("主键id :" + user.getId());
    }


}
