package com.hyh.mybatisplus;

import com.hyh.mybatisplus.entity.User;
import com.hyh.mybatisplus.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Summerday
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ArTest {


    @Test
    public void insert(){
        User user = new User();
        user.setName("summer");
        user.setCreateTime(new Date());
        user.setEmail("xxx");
        user.setAge(50);
        boolean insert = user.insert();
        System.out.println(insert);
    }


    @Test
    public void selectById(){
        User user = new User();
        User selectById = user.selectById(1319899114158284802L);//新对象
        System.out.println(selectById == user);
        System.out.println(selectById);
    }

    @Test
    public void selectById2(){
        User user = new User();
        user.setId(1319899114158284802L);
        User selectById = user.selectById();//新对象
        System.out.println(selectById == user);
        System.out.println(selectById);
    }

    @Test
    public void updateById(){
        User user = new User();
        user.setId(1319899114158284802L);
        user.setAge(30);
        user.setEmail("xxxxxxx");
        boolean b = user.updateById();//新对象
        System.out.println(b);
    }

    @Test
    public void deleteById(){
        User user = new User();
        user.setId(1319899114158284802L);
        boolean b = user.deleteById();//新对象
        System.out.println(b);
    }


    @Test
    public void insertOrUpdate(){
        User user = new User();
        user.setId(1319899114158284802L);
        user.setAge(30);
        user.setEmail("xxxxxxx");

        boolean b = user.insertOrUpdate();
        System.out.println(b);
    }
}
