package com.hyh.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.hyh.mybatisplus.entity.User;
import com.hyh.mybatisplus.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.lang.management.ManagementFactory;

/**
 * @author Summerday
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UpdateTest {

    @Resource
    private UserMapper userMapper;

    @Test
    public void updateById(){
        User user = new User();
        user.setId(1319853504134770689L);
        user.setAge(25);
        user.setName("summerday");
        int row = userMapper.updateById(user);
        System.out.println("影响记录数 : " + row);
    }

    @Test
    public void updateByWrapper(){
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("name","summerday").eq("age",25);
        User user = new User();
        user.setEmail("hyh1332@qwe.com");
        user.setAge(26);
        int row = userMapper.update(user, updateWrapper);
        System.out.println("影响记录数 : " + row);
    }

    /**
     * 更新少量字段
     */
    @Test
    public void updateByWrapperField(){
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("name","summerday").set("age",40);
        int row = userMapper.update(null, updateWrapper);
        System.out.println("影响记录数 : " + row);
    }


}
