package com.hyh.shiro.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Summerday
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserBean implements Serializable {

    private String username;

    private String password;

    private String role;

    private String permission;

    private Boolean locked;

    public boolean isLocked(){
        return locked;
    }

}
