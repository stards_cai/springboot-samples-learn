package com.hyh.simple;

import com.hyh.simple.observe.UserRegisterSubject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Summerday
 */

@RestController
@RequestMapping("/user")
public class SimpleUserController {

    @Autowired
    UserRegisterSubject subject;
    @Autowired
    private SimpleEmailService emailService;
    @Autowired
    private SimpleCouponService couponService;
    @Autowired
    private SimpleUserService userService;

    @GetMapping("/register")
    public String register(String username) {
        // 注册
        userService.register(username);
        // 发送邮件
        emailService.sendEmail(username);
        // 发送优惠券
        couponService.addCoupon(username);
        return "注册成功!";
    }

    @GetMapping("/reg")
    public String reg(String username) {
        userService.register(username);
        subject.notifyObservers(username);
        return "success";
    }
}
