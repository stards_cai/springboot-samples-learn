package com.hyh.simple;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Summerday
 */
@Slf4j
@Service
public class SimpleCouponService {

    ExecutorService executorService = Executors.newSingleThreadExecutor();

    public void addCoupon(String username) {
        executorService.execute(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.info("给用户 [{}] 发放优惠券", username);
        });
    }
}
