package com.hyh.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Spring Task配置类
 * @author Summerday
 */
@Configuration
@EnableScheduling  // 同步
@EnableAsync // 异步
public class ScheduleConfig {

}
