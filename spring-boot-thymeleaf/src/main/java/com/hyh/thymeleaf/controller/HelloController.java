package com.hyh.thymeleaf.controller;

import com.hyh.thymeleaf.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Summerday
 */

@Controller
public class HelloController {

    @GetMapping("/")
    public String index(ModelMap map){
        map.addAttribute("url","/list");
        map.addAttribute("msg","点我点我");
        return "index";
    }

    @GetMapping("/user")
    public String getOne(HttpServletRequest request){
        User user = new User("1","hyh",15);
        request.setAttribute("user",user);
        return "user";
    }

    @GetMapping("/list")
    public String list(Model model){
        List<User> users = new ArrayList<>();
        users.add(new User(UUID.randomUUID().toString(),"summerday",20));
        users.add(new User(UUID.randomUUID().toString(),"天乔巴夏",18));
        model.addAttribute("users",users);
        return "list";
    }

    @ResponseBody
    @GetMapping("/test")
    public String test(){
        return "test";
    }
}
