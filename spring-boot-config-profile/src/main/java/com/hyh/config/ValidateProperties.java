package com.hyh.config;

import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.PublicKey;

/**
 * @author Summerday
 */

@Data
@Validated
@ConfigurationProperties(prefix = "validate")
public class ValidateProperties {

    @NotNull
    private String name;

    @Valid
    private final SubProperties subProperties = new SubProperties();


    @Data
    public static class SubProperties {

        @Min(value = 10, message = "年龄最小为10")
        public Integer age;
    }
}
