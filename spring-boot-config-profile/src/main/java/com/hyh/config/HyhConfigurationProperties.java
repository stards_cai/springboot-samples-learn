package com.hyh.config;

import lombok.Data;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Summerday
 */

@ConfigurationProperties(prefix = "hyh")
public class HyhConfigurationProperties {

    private String username;
    private String password;

    private final Map<String, String> map = new HashMap<>();

    public Map<String, String> getMap() {
        return map;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("{");
        sb.append("\"username\":\"")
                .append(username).append('\"');
        sb.append(",\"password\":\"")
                .append(password).append('\"');
        sb.append(",\"map\":")
                .append(map);
        sb.append('}');
        return sb.toString();
    }
}
