package com.hyh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
//@EnableConfigurationProperties({HyhConfigurationProperties.class, MyProperties.class,AcmeProperties.class})
@ConfigurationPropertiesScan({"com.hyh.config"})
public class SpringBootProfileApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootProfileApplication.class, args);
    }
}
