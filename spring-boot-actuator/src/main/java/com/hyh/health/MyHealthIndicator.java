package com.hyh.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class MyHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        // perform some specific health check
        boolean check = check();
        if (!check) {
            return Health.down().withDetail("Error Code", 0).build();
        }
        return Health.up().build();
    }


    private boolean check() {
        return new Random().nextBoolean();
    }

}